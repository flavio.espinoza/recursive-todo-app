import { combineReducers } from 'redux';
import ToDosReducer from './reducer_todos';
import { reducer as modalReducer } from 'redux-saga-modal'

const rootReducer = combineReducers({
  todoList: ToDosReducer,
  modals: modalReducer,
});

export default rootReducer;
