import React from 'react'

function NewTodoInput(props) {
  const [task, setTask] = React.useState('')
  const handleInputChange = (text) => {
    setTask(text)
    props.onTextChange(text)
  }
  return (
    <div className={'mx-2 rounded-md'}>
      <input
        className={'p-2'}
        type='text'
        placeholder='new task'
        value={task}
        onChange={(e) => handleInputChange(e.target.value)}
      />
    </div>
  )
}

export default NewTodoInput