import React from 'react'
import _ from 'lodash'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

function ModalAddTask(props) {
  const [task, setTask] = React.useState('')
  const [showModal, setShowModal] = React.useState(false)

  const handleChange = (e) => {
    setTask(e.target.value)
  }

  const handleClose = () => {
    const taskNew = {
      id_parent: props.id_parent,
      task: task,
      timestamp: _.now(),
    }
    props.handleAddTask(taskNew)
    props.setOpen(false)
  }

  return (
    <div>
      <Dialog
        open={showModal}
        onClose={handleClose}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>Subscribe</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To subscribe to this website, please enter your email address here.
            We will send updates occasionally.
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='new_task'
            label='New Task'
            type='text'
            fullWidth
            value={task}
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='primary'>
            Cancel
          </Button>
          <Button onClick={handleClose} color='primary'>
            Subscribe
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default ModalAddTask
