import React from 'react'
import { PlusCircleIcon, TrashIcon } from '@heroicons/react/solid'

function TodoListItem(props) {
  return (
    <li className={'p-4 mb-2 shadow-md bg-white'}>
      <div className={'flex flex-row justify-between items-center'}>
        <span className={'text-xs text-gray-400'}>{props.todo.id}</span>
        <span>{props.todo.content}</span>
        <span className={'flex items-center'}>
          <button className={'mr-4 cursor-pointer'} onClick={() => props.addChildTask(props.todo.id)}>
            <PlusCircleIcon className='h-5 w-5 text-blue-300 hover:text-blue-500' />
          </button>
          <button onClick={() => props.deleteTodo(props.todo.id)}>
            <TrashIcon className='h-5 w-5 text-red-300 hover:text-red-500' />
          </button>
        </span>
      </div>
    </li>
  )
}

export default TodoListItem
