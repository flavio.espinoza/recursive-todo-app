import axios from 'axios'
import {
  call,
  fork,
  all,
  select,
  put,
  take,
  takeEvery,
  race,
} from 'redux-saga/effects';

const API_DOMAIN = 'next.json-generator.com'
const API_ROUTE = 'json/get/'
const API_KEY = '4kNppKLB5'

const API_URL = 'https://next.json-generator.com/api/json/get/VkHT1oUSq'

export function* fetchTodos() {
  try {
    const db = yield call(axios.get, 'postgresql://db:s68i6chhcnz3zc43@app-3f088806-4583-429e-a220-85f096b081f2-do-user-3507157-0.b.db.ondigitalocean.com:25060/db?sslmode=require')
    const todos = yield call(axios.get, `https://${API_DOMAIN}/api/${API_ROUTE}/${API_KEY}`)
    const user = yield call(axios.get, API_URL)
    console.log(JSON.stringify(user))
    yield put({ type: 'FETCH_TODOS_SUCCESS', data: user.data.todos })
  } catch (error) {
    console.log('fetchTodos error:', error.message)
  }
}

function* watchFetchTodos() {
  yield takeEvery('FETCH_TODOS', fetchTodos)
}

export function* createTodo(action) {
  const newTodo = { title: '', categories: '', content: action.payload }
  try {
    yield call(axios.post, `${API_URL}/posts${API_KEY}`, newTodo)
    yield put({ type: 'FETCH_TODOS' })
  } catch (error) {
    console.log('createTodo error:', error.message)
  }
}

function* watchAddToDo() {
  yield takeEvery('ADD_TODO', createTodo)
}

export function* deleteTodo({ id }) {
  try {
    yield call(axios.delete, `${API_URL}/posts/${id}${API_KEY}`)
    yield put({ type: 'FETCH_TODOS' })
  } catch (error) {
    console.log('deleteTodo Error:', error.message)
  }
}

function* watchDeleteTodo() {
  yield takeEvery('DELETE_TODO', deleteTodo)
}

export default function* rootSaga() {
  yield all([watchFetchTodos(), watchAddToDo(), watchDeleteTodo()])
}
