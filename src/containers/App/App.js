import React, { Component } from 'react'
import { connect } from 'react-redux'
import { hot } from 'react-hot-loader'

import AddTodoButton from '../../components/AddTodoButton/add_todo_button'
import NewTodoInput from '../../components/NewTodoInput/new_todo_input'
import ToDoList from '../TodoList/to_do_list'

import ModalAddTask from '../../components/Modal/ModalAddTask'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      newTodoText: '',
      user: {
        preferredName: 'Flavio',
      },
      env: null,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleTodoTextChange = this.handleTodoTextChange.bind(this)
    this.handleAddChildTask = this.handleAddChildTask.bind(this)
  }

  componentDidUpdate(prev, current) {
    console.log('prev', prev)
  }

  handleAddChildTask(id) {
    // @todo : add child task
    console.log('id ------------ ', id)
    this.setState({
      showModal: true,
    })
  }

  handleTodoTextChange(text) {
    this.setState({
      newTodoText: text,
    })
  }

  handleSubmit() {
    this.props.dispatch({ type: 'ADD_TODO', payload: this.state.newTodoText })
    // @fix : set newTodoText to ''
    this.setState({
      newTodoText: '',
    })
  }

  render() {
    const { todoList } = this.props
    console.log('this.props', this.props)
    // console.log(JSON.stringify(todoList, null, 2))
    return (
      <div className={'p-12'}>
        <h1>{`${this.state.user.preferredName}'s Awesome Todo List`}</h1>
        <div className={'flex flex-row-reverse'}>
          <NewTodoInput onTextChange={this.handleTodoTextChange} />
          <AddTodoButton onClick={this.handleSubmit} />
        </div>
        <ToDoList todos={todoList} addChildTask={this.handleAddChildTask} />
        <ModalAddTask open={this.state.showModal} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    todoList: state.todoList,
  }
}

export default hot(module)(connect(mapStateToProps)(App))
