import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { deletePost, fetchTodos } from '../../actions'
import ToDoItem from '../../components/ToDoItem/todo_item'
import ModalAddTask from '../../components/Modal/ModalAddTask'

class ToDoList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
    }
    this.deleteTodo = this.deleteTodo.bind(this)
    this.addChildTask = this.addChildTask.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  componentDidMount() {
    this.props.fetchTodos()
  }

  deleteTodo(id) {
    this.props.deletePost(id)
  }

  openModal(id) {
    this.setState({
      showModal: true,
    })
  }

  closeModal() {}

  addChildTask(id) {
    this.props.addChildTask(id)
  }

  render() {
    const { todos = [] } = this.props
    return todos.length ? (
      <section>
        <ul>
          {todos.map((obj) => (
            <ToDoItem
              deleteTodo={this.deleteTodo}
              addChildTask={this.addChildTask}
              key={obj.id}
              todo={obj}
            />
          ))}
        </ul>
      </section>
    ) : (
      <div>Loading</div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ deletePost, fetchTodos }, dispatch)
}

export default connect(null, mapDispatchToProps)(ToDoList)
